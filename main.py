from persons_generator import PersonDbGenerator

database_1 = PersonDbGenerator("mydatabase1.db")
database_1.create_table("USERS_TABLE", "USER_ID", "USER_NAME", "USER_SURNAME")
database_1.add_to_table(database_1.generate_persons('male', 10), "USERS_TABLE")
database_1.conn_db.commit()
