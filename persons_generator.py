from openpyxl import load_workbook
import random
import sqlite3


class PersonDbGenerator:
    db = ""
    cursor = None
    conn_db = None

    def __init__(self, db):
        self.db = db
        self.conn_db = sqlite3.connect(self.db)

    @staticmethod
    def generate_persons(sex: str, number: int) -> list:
        result = []
        ws1 = None
        ws2 = None
        if sex == 'male':
            wb_name = load_workbook(filename='male_names_pl.xlsx')
            wb_surname = load_workbook(filename='male_surnames_pl.xlsx')
            ws1 = wb_name.active
            ws2 = wb_surname.active
        elif sex == 'female':
            wb_name = load_workbook(filename='female_names_pl.xlsx')
            wb_surname = load_workbook(filename='female_surnames_pl.xlsx')
            ws1 = wb_name.active
            ws2 = wb_surname.active

        for m in range(number):
            temp = ws1.cell(row=random.randint(2, 500), column=1).value + " " + ws2.cell(
                row=random.randint(2, ws2.max_row),
                column=1).value
            result.append(temp)

        return result

    def add_to_table(self, person_list, table):
        for person in person_list:
            person = person.split()
            self.cursor.execute('''INSERT into {} values(?,?,?)'''.format(table), (None, person[0], person[1]))

    def create_table(self, table_name: str, id: str, first_col: str, sec_col: str):
        self.cursor = self.conn_db.cursor()
        self.cursor.execute(
            '''CREATE TABLE {} ({} integer PRIMARY KEY AUTOINCREMENT NOT NULL, {} text, {} text)'''.format(table_name,
                                                                                                           id,
                                                                                                           first_col,
                                                                                                           sec_col))
